import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:docapp/shared/constants.dart';
import 'package:url_launcher/link.dart';
import 'package:url_launcher/url_launcher.dart';
Future<void> _launchUrl() async {
 if (!await launchUrl(url)) {
   throw 'Could not launch $url';
 }

}

Future<void> firebaseMessagingBackgroundHandler(message) async {
await Firebase.initializeApp();
_launchUrl();



}
final Uri url = Uri.parse('https://www.instagram.com/eva_mariage/');
Future <void> main() async{
WidgetsFlutterBinding.ensureInitialized();

if(kIsWeb){
await Firebase.initializeApp(
    options: FirebaseOptions(
        apiKey: Constants.apiKey,
        appId: Constants.appId,
        messagingSenderId: Constants.messagingSenderId,
        projectId: Constants.projectId));
}

else{
await Firebase.initializeApp();
}
FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);


runApp(const MyApp());
launchUrl(url);
}

class MyApp extends StatelessWidget {
const MyApp({super.key});


@override
Widget build(BuildContext context) {
return MaterialApp(

  title: 'Flutter Demo',
  theme: ThemeData(
  
    primarySwatch: Colors.blue,
  ),
  //home: const MyHomePage(title: 'Flutter Demo Home Page'),
);
}
}

class MyHomePage extends StatefulWidget {
const MyHomePage({super.key, required this.title});



final String title;

@override
State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
int _counter = 0;
@override void initState() {
init();
super.initState();
}
init( )  async { String deviceToken = await getDeviceToken();
print(deviceToken);}

void _incrementCounter() {
setState(() {
  
  _counter++;
});
}

 @override
Widget build(BuildContext context) {

return Scaffold(
  appBar: AppBar(
    
    title: Text(widget.title),
  ),
  body: Center(
    
    child: Column(
        
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        
      
        ],
        ),
    ),
    
    );
}


}


Future getDeviceToken () async {
 FirebaseMessaging firebasemessage = FirebaseMessaging.instance;
String? deviceToken = await firebasemessage.getToken();
    return(deviceToken==null) ? "":deviceToken;
    }