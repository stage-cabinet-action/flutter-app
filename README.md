# Documentation technique n°1
rédiger par Sebastien Mercellus , Yanis Laakad et  Nils Dargent 

version 1.0

<style> #prerequis{color :#ea0047} h1,h2,#chapitre1{text-align : center} #conseil{color : #ea0047} p{font-size : 1.4rem}</style>

<h2 id="prerequis">Prérequis !!! avoir une version de flutter à jour installé sur le poste et visual studio code </h2>

## Sommaire


-  ## <a href="#chapitre1"> Crée une appliaction flutter </a>
- ### crée application
- ### changer le logo
- ### changer le lien de redirection
- ##  Crée un compte google firebase
- ##  Lier un notre application à notre compte Google firebase
- ##  Comment build l'application flutter pour android


<h2 id="chapitre" >chapitre 1 </h2>

 <p>ouvrez votre ide Visual studio code et cliquez sur view</p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen1.png">

 <p>selectionnez commande palette</p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen2.png">

 <p>puis cliquez sur <strong>application</strong> vscode va vous demander ou voulez vous crée vôtre dossier</p>
 <p id="conseil"> conseil : crée un nouveau dossier pour y mettre votre application</p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen3.png">

 <p>choisissez un nom pour votre application et appuyer sur entrer</p>

 <p id ="conseil"> conseil : prendre un nom explicite pour votre application  </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen4.png">
 <p> copier le code ci-desous</p>

    import 'package:firebase_core/firebase_core.dart';
    import 'package:firebase_messaging/firebase_messaging.dart';
    import 'package:flutter/material.dart';
    import 'package:flutter/foundation.dart';
    import 'package:eva_mariage/shared/constants.dart';
    import 'package:url_launcher/link.dart';
    import 'package:url_launcher/url_launcher.dart';
    Future<void> _launchUrl() async {
     if (!await launchUrl(url)) {
       throw 'Could not launch $url';
     }

    }

    Future<void> firebaseMessagingBackgroundHandler(message) async {
    await Firebase.initializeApp();
    _launchUrl();
  
  
  
    }
    final Uri url = Uri.parse('https://www.instagram.com/eva_mariage/');
    Future <void> main() async{
    WidgetsFlutterBinding.ensureInitialized();

    if(kIsWeb){
    await Firebase.initializeApp(
        options: FirebaseOptions(
            apiKey: Constants.apiKey,
            appId: Constants.appId,
            messagingSenderId: Constants.messagingSenderId,
            projectId: Constants.projectId));
    }

    else{
    await Firebase.initializeApp();
    }
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
   

    runApp(const MyApp());
    launchUrl(url);
    }

    class MyApp extends StatelessWidget {
    const MyApp({super.key});

  
    @override
    Widget build(BuildContext context) {
    return MaterialApp(

      title: 'Flutter Demo',
      theme: ThemeData(
      
        primarySwatch: Colors.blue,
      ),
      //home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
    }
    }

    class MyHomePage extends StatefulWidget {
    const MyHomePage({super.key, required this.title});

  

    final String title;

    @override
    State<MyHomePage> createState() => _MyHomePageState();
    }

    class _MyHomePageState extends State<MyHomePage> {
    int _counter = 0;
    @override void initState() {
    init();
    super.initState();
    }
    init( )  async { String deviceToken = await getDeviceToken();
    print(deviceToken);}

    void _incrementCounter() {
    setState(() {
      
      _counter++;
    });
    }

     @override
    Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        
        title: Text(widget.title),
      ),
      body: Center(
        
        child: Column(
            
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            
          
            ],
            ),
        ),
        
        );
    }


    }


    Future getDeviceToken () async {
     FirebaseMessaging firebasemessage = FirebaseMessaging.instance;
    String? deviceToken = await firebasemessage.getToken();
        return(deviceToken==null) ? "":deviceToken;
        }
 <p>Aprés avoir copier le code remplacer le par le contenu du fichier main.dart </p>
 <p id ="conseil">le fichier main.dart se trouve à dans le dossier lib qui est à la racine du projet </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen5.png">

 #### Partie changer le logo 
 <p id ="conseil"> Créer un dossier "asset" à la racine du projet </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/CL1.png">
 <p id ="conseil"> Mettre l'image à l'intérieur, elle doit obligatoirement de taille 1024x1024 pixel.</p>
 <p id ="conseil"> Ajouter dans le fichier pubspec.yaml, dans la partie dev_dependencies, et au même niveau que flutter_test, la ligne :</p>

    flutter_laucher_icons:^0.11.0

 <p id ="conseil">Où une version plus récente si il y a.</p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/CL2.png">
 <p id ="conseil">Puis faire Ctrl+S en selectionnant la ligne rajouter ou cliquer sur cette icône pour installer les packages</p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/CL3.png">
 <p id ="conseil">A la suite et au même niveau que dev_dependencies, ajouté ce code</p>

    flutter_icons:
      android:"laucher_icon"
      ios: true 
      image_path:"asset/<<nom de l'image>>.png

 <p id ="conseil">Dans un terminal ouvert à la racine du projet entrée la commande</p>

    Flutter pub run flutter_launcher_icons:main
    
 <p id ="conseil">Relancer l'application et l'icon aura changer.</p>

 <h3>changer le lien de redirection de l'application </h2>

  <p> dans le fichier main.dart en paramètre de la fonction uri.pars() àjouter y en chaine de caracère l'url dans lequel vous voulez rediriger l'utilisateur lorque vous entrez dans l'application</p>
   <img title="a title" alt="capture ecran 1" src="Documentation technique/screen14.png">
 

  <h3> Résolution erreur </h2>

  <p> dans le fichier build.gradle qui se trouve dans cette emplacement </p>
  <img title="a title" alt="capture ecran 1" src="Documentation technique/screen21.png">
  <img title="a title" alt="capture ecran 1" src="Documentation technique/screen19.png">
  <p>effectuer les modification suivante  </p>
  <p>avant </p>
  <img title="a title" alt="capture ecran 1" src="Documentation technique/screen18.png">
  <p>après </p>
  <img title="a title" alt="capture ecran 1" src="Documentation technique/screen15.png">
  <p>puis celle-ci </p>
  <p>avant </p>
  <img title="a title" alt="capture ecran 17" src="Documentation technique/screen17.png">
  <p>après </p>
  <img title="a title" alt="capture ecran 1" src="Documentation technique/screen16.png">
 
 ## crée un compte google firebase
 ### introduction 
  <p>google firebase est un service proposé par google qui dans nôtre cas il va nous permettre de mettre en place un système de notification push à envoyer à tout les appareils possèdant application.</p>
  <p>l'ajoue du code permettant d'envoyer des notification push à deja été écrit  par nos soins et si vous avez suivie les étapes rigoureusement vous avez deja coller le code dans le main.dart et installer les packages firebase core et firebase messaging </p>
<p> cependant il néccessite d'effectuer des étapes suplémentaires afin que le service de notification push fonctionne. Nous allons les voir ensemble <p/>

 #### crée une projet google firbase
 <p> connecter vous à google firebase grâce à vôtre compte google </p>
  <p>une fois connecté vous êtes rediriger sur la page accueil google firebase   </p>
   <p id ="conseil">cliquez sur get starded </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen22.png"> 
 <p id ="conseil">cliquer*z sur ajouter un projet  </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen23.png"> 
  <p> nommer vôtre projet et cliquez sur continuer </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen24.png"> 
   <p> à un projet on y associe une ou plusieurs application nous allons voir comment en ajouter une </p>
   <p> cliquez sur icone android </p>
   <p id="conseil">vous n'êtes pas obligé de recrée pour chaque application un nouveau projet un projet principale avec plusieurs application suffit. Cependant libre à vous si vous trouver que l'inverse est plus organisé </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/screen25.png"> 

 #### lier votre compte google firebase à vôtre application
  <p>afin d'ajouter le sdk firebase je nous vous recommandons d'utiliser la documentation officiel </p>
  <a href="https://firebase.google.com/docs/android/setup?hl=fr" > documentation</a>
   <p>https://firebase.google.com/docs/android/setup?hl=fr </p>

<h3> Build </h3>

 <p> Maintenant que l'application est fini d'être codé il suffit de le créer pour l'installer sur des téléphones </p>
 <p> Pour cela il faut build l'application et il y a 2 méthodes:
 <p> 1- Cliquer sur le bouton suivant.
 <img title="a title" alt="capture ecran 1" src="Documentation technique/Build1.png">
 <p> 2- Entrer la commande ci-dessous dans un terminal ouvert à la racine du projet </p>

    flutter build apk 

 <p> Et maintenant qu'il est crée, il faut allé chercher le fichier app.apk dans le dossier ci-dessous et le mettre sur votre téléphone </p>
 <img title="a title" alt="capture ecran 1" src="Documentation technique/Build2.png"> 
